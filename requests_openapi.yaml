openapi: 3.0.0
info:
  version: 1.0.0
  title: Управление заявками пользователя

security:
  - bearerAuth: []

paths:
  '/api/its/v1/requests':
    get:
      parameters:
        - $ref: '#/components/parameters/page'
        - $ref: '#/components/parameters/limit'
      summary: Получение заявок пользователя
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/RequestData'
        '401':
          $ref: '#/components/responses/ResponseError'
        '500':
          $ref: '#/components/responses/ResponseError'

    post:
      summary: Создание новой заявки пользователя
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RequestNewData'
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RequestData'
        '400':
          $ref: '#/components/responses/ResponseError'
        '401':
          $ref: '#/components/responses/ResponseError'
        '500':
          $ref: '#/components/responses/ResponseError'

  '/api/its/v1/requests/{id}':
    parameters:
      - in: path
        name: id
        required: true
        schema:
          type: number
          example: 234
    get:
      summary: Получение заявки по её id в БД Портала
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/RequestData'
        '400':
          $ref: '#/components/responses/ResponseError'
        '401':
          $ref: '#/components/responses/ResponseError'
        '403':
          $ref: '#/components/responses/ResponseError'
        '404':
          $ref: '#/components/responses/ResponseError'
        '500':
          $ref: '#/components/responses/ResponseError'


components:

  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT

  parameters:
    page:
      in: query
      name: page
      required: false
      description: номер страницы возвращаемой части списка
      schema:
        type: integer
        format: int64
        minimum: 0
        default: 0
    limit:
      in: query
      name: limit
      required: false
      description: количество возвращаемых записей на страницу
      schema:
        type: integer
        format: int64
        minimum: 1
        default: 20

  responses:
    ResponseError:
      description: Ответ в случае ошибки
      content:
        application/json:
          schema:
            type: object
            properties:
              error:
                type: object
                properties:
                  status:
                    type: number
                    format: int32
                  timestamp:
                    description: Дата и время ответа метода
                    type: string
                    format: date-time
                  errors:
                    type: array
                    items:
                      type: object
                      properties:
                        messages:
                          description: Массив строк с сообщением об ошибке для пользователя
                          type: array
                          items:
                            type: string
                        path:  
                          type: string
                          description: Идентификатор поля, которое содержит ошибочное значение ошибки
          examples:
            400:
              value: 
                {"error": {"status": 400,"timestamp": "2020-11-06T10:10:14.166Z","errors": [{"messages": ["Должно быть задано количество записей на страницу"],"path": "limit"}]}}
            401:
              value: 
                {"error": {"status": 401,"timestamp": "2020-11-06T10:10:14.166Z","errors": [{"messages": ["Неверное имя или пароль"]}]}}
            403:
              value: 
                {"error": {"status": 403,"timestamp": "2020-11-06T10:10:14.166Z","errors": [{"messages": ["Недостаточно прав доступа"]}]}}
            404:
              value: 
                {"error": {"status": 404,"timestamp": "2020-11-06T10:10:14.166Z","errors": [{"messages": ["Заявка не найдена"]}]}}
            500:
              value: 
                {"error": {"status": 500,"timestamp": "2020-11-06T10:10:14.166Z","errors": [{"messages": ["Неизвестная ошибка"]}]}}


  schemas:
    RequestNewData:
      description: Объект для создания заявки
      type: object
      properties:
        subject:
          description: Тема заявки
          type: string
          example: 'Пополнение локального склада (включая расходники)'
        templateId:
          description: id шаблона заявки 4me (сервис)
          type: number
          format: integer
          example: 4
        attachments:
          description: файлы-вложения в заявку
          type: boolean
          example: true
        requestedForId:
          description: id пользователя - получателя услуги
          type: number
          format: integer
          example: 333
        service_instance_id:
          description: id инстанса услуги
          type: number
          format: integer
          example: 34
        customFields:
          description: Дополнительные поля, В соответствтсвии с выбранным для заявки шаблоном (сервисом)
          type: array
          items:
            type: object

    RequestData:
      description: Объект при чтении заявки
      type: object
      properties:
        id:
          description: Идентификатор заявки
          type: number
          format: integer
          example: 3433
        externalId:
          description: Идентификатор заявки в СМКСС
          type: number
          format: integer
          example: 2354
        subject:
          description: Тема заявки
          type: string
          example: 'Пополнение локального склада (включая расходники)'
        category:
          description: Категория заявки
          type: string
          enum: ['INCIDENT', 'RFC', 'RFI', 'RESERVATION', 'COMPLAINT', 'COMPLIMENT', 'OTHER']
          example: 'RFC'
        status:
          description: Статус заявки в 4me
          type: string
          enum: ['DECLINED', 'ASSIGNED', 'ACCEPTED', 'IN_PROGRESS', 'WAITING_FOR', 'WAITING_FOR_CUSTOMER', 'CHANGE_PENDING', 'PROJECT_PENDING', 'COMPLETED']
          example: 'COMPLETED'
        completion_reason:
          description: Причина закрытия заявки в 4me, дополняет статус COMPLETED
          type: string
          enum: ['SOLVED', 'WORKAROUND', 'GONE', 'DUPLICATE', 'WITHDRAWN', 'NO_REPLY', 'REJECTED', 'CONFLICT', 'DECLINED', 'UNSOLVABLE']
          example: 'SOLVED'
        created_at:
          description: Дата и время создания
          type: string
          format: date-time
          example: '2020-11-11T20:59:59.000+03:00'
        updated_at:
          description: Дата и время последнего обновления заявки (включая добавление комментариев)
          type: string
          format: date-time
          example: '2020-11-21T20:59:59.000+03:00'
        completed_at:
          description: Дата и время закрытия заявки
          type: string
          format: date-time
          example: '2020-11-20T20:59:59.000+03:00'
        hasAttachments:
          description: Признак наличия файлов-вложений в заявку
          type: boolean
          example: true
        requestedByName:
          description: ФИО пользователя, запросившего услугу
          type: string
          example: 'Бобриков Иван Федорович'
        requestedForName:
          description: ФИО пользователя - получателя услуги
          type: string
          example: 'Глебов Артём Александрович'
        service_instance_name:
          description: Наименование услуги
          type: string
          example: 'Пополнение локального склада (включая расходники)'
        last_note_text:
          description: Текст последнего комментария к заявке
          type: string
          example: 'Отправлено в понедельник'
