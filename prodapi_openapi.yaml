openapi: 3.0.1
info:
  title: 1.00 Продкаталог
  description: Описание возможных точек взаимодействия
  version: 1.0.0
servers:
  - url: https://xxxxxxx/v1
    description: test 
  - url: https://xxxxxxxb/xxxxxxx/v1
    description: ПРОД 


paths:
  /productspec/{id}:
    parameters:
      - in: path 
        name: id
        description: Идентификатор продуктовой спецификации
        required: true
        schema:
          type: string
        
    get:
      summary: Получение продуктовой спецификации
      operationId: getProductSpec
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductSpec'
        '400':
          $ref: '#/components/responses/ErrorResponse'
        '401':
          $ref: '#/components/responses/ErrorResponse'
        '403':
          $ref: '#/components/responses/ErrorResponse'
        '500':
          $ref: '#/components/responses/ErrorResponse'

  /productoffer/{id}:
    parameters:
      - in: path 
        name: id
        description: Идентификатор продуктового предложения
        required: true
        schema:
          type: string
        
    get:
      summary: Получение продуктового предложения
      operationId: getProductOffer
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ProductOffer'
        '400':
          $ref: '#/components/responses/ErrorResponse'
        '401':
          $ref: '#/components/responses/ErrorResponse'
        '403':
          $ref: '#/components/responses/ErrorResponse'
        '500':
          $ref: '#/components/responses/ErrorResponse'

components:
  responses:
    ErrorResponse:
      description: Ответ метода при ошибке
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorData'

  schemas:
    ErrorData:
      type: object
      properties:
        type:
          description: Тип ошибки
          type: string
          enum:
          - syntax_error
          - unauthorized
          - generic_error
          - user_exists
          - user_not_found
          - user_locked
          example: user_locked
        messageTitle:
          description: Заголовок ошибки
          type: string
          maxLength: 255
          minLength: 0
          example: 'Ошибка'
        message:
          description: Текст ошибки
          type: string
          maxLength: 255
          minLength: 0
          example: 'Пользователь заблокирован'
        additionalProperties:
          type: object
          example: {}
  
    Property:
      type: object
      properties:
        slug:
          type: string
          description: Слаг свойства
          maxLength: 20
          minLength: 3
          example: 'diameter'
        name:
          type: string
          description: Имя свойства
          maxLength: 100
          minLength: 3
          example: 'Диаметр'
        type:
          type: string
          description: Тип значения свойства
          enum: [integer, real, string, inline-enum, boolean, table]
        table:
          type: string
          description: Слаг таблицы, откуда берутся значения для типа table
        value:
          type: string
          description: Значение свойства в рантайме
        defaultValue:
          type: string
          description: Значение свойства по умолчанию

    Component:
      type: object
      properties:
        slug:
          type: string
          description: Слаг компонента
          maxLength: 20
          minLength: 3
          example: 'wheel'
        name:
          type: string
          description: Имя компонента
          maxLength: 100
          minLength: 3
          example: 'Колесо'
        properties:
          type: array
          description: Список свойств компонента
          items: 
            $ref: '#/components/schemas/Property'
        
    ComponentTreeNode:
      type: object
      description: Узел включения компонента или дочерней спецификации в дерево компонентов
      properties:
        component:
          $ref: '#/components/schemas/Component'
        propertiesValues:
          description: Переопределение диапазонов допустимых значений для свойств компонента
          type: array
          items:
            type: object
            description: Сопоставление трех сущностей - слаг свойства - диапазон значений - список правил применимости для этого диапазона
            properties:
              propertySlug:
                description: Слаг свойства
                type: string
              propertyValuesRanges:
                $ref: '#/components/schemas/PropertyValuesRanges'
        rules:
          description: Правила применимости и совместимости, заданные для компонента в дереве
          type: array
          items:
            $ref: '#/components/schemas/Rules'
        childNodes:
          type: array
          description: Список дочерних узлов для данного узла дерева. Тут надо немного бует изменить - указать тип узла (узел - либо компонент либо слаг спецификации)
          items:
            oneOf:
              - $ref: '#/components/schemas/ComponentTreeNode'
              - $ref: '#/components/schemas/ProductSpec'

    Rules:
      type: array
      description: Список правил
      items:
        type: object
        description: Правило применимости или совместимости для компонента, значения свойства компонента, цены или скидки. Представляет из себя ДНФ, записанную из термов в строку и массив определений для термов.
        properties:
          DNF:
            type: string
          terms:
            type: array
            description: Терм в ДНФ. Например, это выражение вида значение свойства компонента равно константе (конкретному значению)
            items:
              type: object
              properties:
                termType: 
                  type: string
                  enum: ['Property', 'Geo']
                componentSlug:
                  type: string                
                propertySlug:
                  type: string
                operator:
                  type: string
                  description: Оператор сравнения
                  enum: ['in', 'eq', 'neq', 'le', 'lte', 'ge', 'gte']
                value:
                  description: Значение свойства для терма
                  oneOf:
                    - $ref: '#/components/schemas/BooleanRange'
                    - $ref: '#/components/schemas/TableRange'
                    - $ref: '#/components/schemas/NumberRange'
                    - $ref: '#/components/schemas/IntegerRange'
                    - $ref: '#/components/schemas/StringRange'

    PropertyValuesRanges:
      type: array
      description: Список допустимых диапазонов значений свойства
      items:
        type: object
        description: Диапазон значений свойства (зависит от типа свойства) вместе со своим правилом применимости
        properties:
          range:
            oneOf:
              - $ref: '#/components/schemas/BooleanRange'
              - $ref: '#/components/schemas/TableRange'
              - $ref: '#/components/schemas/NumberRange'
              - $ref: '#/components/schemas/IntegerRange'
              - $ref: '#/components/schemas/StringRange'
          rules:
              $ref: '#/components/schemas/Rules'
        

    BooleanRange:
      type: object
      description: Диапазон значений свойства логического типа
      properties:
        value:
          type: string
          example: 'false'

    TableRange:
      type: object
      description: Диапазон значений свойства, которые взяты из таблицы БД
      properties:
        values:
          type: array
          uniqueItems: true
          items:
            type: object
            description: массив пар идентификатор-значение из таблицы БД

    NumberRange:
      type: object
      description: Диапазон значений свойства типа действительно число
      properties:
        min:
          type: number
        max:
          type: number

    IntegerRange:
      type: object
      description: Диапазон значений свойства целочисленного типа
      properties:
        min:
          type: integer
        max:
          type: integer

    StringRange:
      type: object
      description: Диапазон значений свойства строкового типа
      properties:
        isRegularExpression:
          type: boolean
        value:
          type: string

    ProductSpec:
      type: object
      description: Продуктовая спецификация (ПС)
      properties:
        createDate:
          type: string
          description: Дата создания ПС
          format: date-time
        updateDate:
          type: string
          description: Дата изменения ПС
          format: date-time
        slug:
          type: string
          description: Слаг ПС
          maxLength: 20
          minLength: 3
          example: 'Car'
        name:
          type: string
          description: Имя ПС
          maxLength: 100
          minLength: 3
          example: 'Колесо'
        components:
          type: array
          description: Дерево компонентов и дочерних спецификаций
          items: 
            type: object
            description: Точка включения компонента или дочерней спецификации в дерево
            items: 
              $ref: '#/components/schemas/ComponentTreeNode'


    ProductOffer:
      type: object
      description: Продуктовое предложение (ПП)
      properties:
        createDate:
          type: string
          description: Дата создания ПП
          format: date-time
        updateDate:
          type: string
          description: Дата изменения ПП
          format: date-time
        slug:
          type: string
          description: Слаг ПП
          maxLength: 20
          minLength: 3
          example: 'BentlyModels2021'
        name:
          type: string
          description: Имя ПП
          maxLength: 100
          minLength: 3
          example: 'Модели Бентли 2021 года'
        
        productsSpecifications:
          type: array
          items:
            $ref: '#/components/schemas/ProductSpec'
        
        childOffers:
          type: array
          description: Массив включенных дочерних ПП
          items:
            type: object
            properties:
              offer:
                $ref: '#/components/schemas/ProductOffer'
              quantity:
                $ref: '#/components/schemas/PropertyValuesRanges'
        
        prices:
          type: array
          description: Дерево цен
          items: 
            type: object
            description: Точка включения цены в дерево
            items: 
              $ref: '#/components/schemas/PriceTreeNode'

    PriceTreeNode:
      type: object
      description: Узел включения цены в дерево цен
      properties:
        nodeType:
          type: string
          enum: ['group', 'simple', 'discountPercent', 'discountAbsolute']
        value:
          description: Величина цены или скидки - число
          type: string
        currency:
          description: Валюта цены - слаг из справочника. Хотя валюта должна задаваться на уровне ПП
          type: string
        componentSlug:
          description: Ссылка на компонент - цена назначается на компонент
          type: string
        rules:
          description: Правила применимости цены
          type: array
          items:
            $ref: '#/components/schemas/Rules'
        childNodes:
          type: array
          description: Список дочерних узлов для данного узла дерева
          items:
            $ref: '#/components/schemas/PriceTreeNode'

